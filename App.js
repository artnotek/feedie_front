import { StatusBar } from 'expo-status-bar';
import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {createBottomTabNavigator} from "react-navigation-tabs";

import {Provider} from 'react-redux';
import { createStore} from "redux";
import rootReducer from './helpers/rootReducer';
import { persistStore, persistReducer } from "redux-persist";
import storage from 'redux-persist/lib/storage';
import { PersistGate } from "redux-persist/integration/react";
import Icon from 'react-native-vector-icons/FontAwesome';
import Splash from "./screens/Splash";
import Login from "./screens/Login";
import Details from "./screens/Details";
import Home from "./screens/Home";
import MyRecipes from "./screens/MyRecipes";
import MyFavorites from "./screens/MyFavorites";
import Search from "./screens/Search";
import Profil from "./screens/Profil";
import NewRecipe from "./screens/NewRecipe";

const persisConfig = {
    key: 'root',
    storage: storage
};
const persistedReducer = persistReducer(persisConfig, rootReducer);
const store = createStore(persistedReducer);
const persistor = persistStore(store);

const BottomNavigator = createBottomTabNavigator({
    Accueil: {
        screen: Home,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon
                name={"home"}
                size={24}
                color={tintColor}
                />
            )
        })
    },
    'Mes plats': {
        screen: MyRecipes,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon
                // name={"file-o"}
                name={"cutlery"}
                size={24}
                color={tintColor}
                />
            )
        })
    },
    Rechercher: {
        screen: Search,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon
                name={"search"}
                size={24}
                color={tintColor}
                />
            )
        })
    },
    'Mes favoris': {
        screen: MyFavorites,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon
                name={"star"}
                size={24}
                color={tintColor}
                />
            )
        })
    },
    Profil: {
        screen: Profil,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon
                name={"user"}
                size={24}
                color={tintColor}
                />
            )
        })
    }

    },{
    tabBarOptions: {
        showLabel: true,
        activeTintColor: '#00ae8f',
        inactiveTintColor: 'grey',
    }
});

const AppNavigator = createStackNavigator(
    {
        Splash: {screen: Splash, navigationOptions: {headerShown: false}},
        Login: {screen: Login, navigationOptions: {headerShown: false}},
        Home: {screen: BottomNavigator, navigationOptions: {headerShown: false}},
        Details: {screen: Details, navigationOptions: {headerShown: false}},
        MyRecipes: {screen: MyRecipes, navigationOptions: {headerShown: false}},
        MyFavorites: {screen: MyFavorites, navigationOptions: {headerShown: false}},
        Search: {screen: Search, navigationOptions: {headerShown: false}},
        Profil: {screen: Profil, navigationOptions: {headerShown: false}},
        NewRecipe: {screen: NewRecipe, navigationOptions: {headerShown: false}},
    },
    {
        initialRouteName: 'Splash'
    }
);

const AppContainer = createAppContainer(AppNavigator);


class App extends Component{
    componentDidMount() {
        console.log('componentdidmount app.js');
    }

    render() {
        return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <AppContainer/>
            </PersistGate>
        </Provider> );
  }

}

export default App;

