const baseUrl =  "localhost:3010";

export  default class UserServices{
     /**
     * function to send a server request to create a new user
     * @param body
     * @returns {Promise<Response>}
     */
    static async create(body)
    {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/users`, init);
        return call;
    }
     /**
     * function to send a server request to create a new user
     * @param body
     * @returns {Promise<Response>}
     */
    static async login(body)
    {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        let call = await  fetch(`${baseUrl}/login`, init);
        return call;
    }

     /**
     * function to send a server request to create a new user
     * @param id
     * @returns {Promise<Response>}
     */
    static async details(id)
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },
        };

        let call = await  fetch(`${baseUrl}/user/${id}`, init);
        return call;
    }

    /**
     * function to send a update request for current user
     * @param id
     * @param body
     * @returns {Promise<Response>}
     */
    static async update(id, body)
    {
        // console.log(body);
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };

        console.log(init);
        let call = await  fetch(`${baseUrl}/user/${id}`, init);
        return call;
    }

    /**
     * function to send a update request for current user
     * @param id
     * @returns {Promise<Response>}
     */
    static async delete(id)
    {
        // console.log(body);
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        };

        console.log(init);
        let call = await  fetch(`${baseUrl}/user/${id}`, init);
        return call;
    }



}
