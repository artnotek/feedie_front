import React, {Component} from 'react';
import {
    View,
    Text, StyleSheet, TouchableOpacity
} from 'react-native';
import tcomb from 'tcomb-form-native';
import UserServices from "../services/UserServices";
import Title from "../components/Title";


// initialisation
const Form = tcomb.form.Form;

// model
const profileModel = tcomb.struct({
    firstname: tcomb.String,
    lastname: tcomb.String,
    email: tcomb.String,
    password: tcomb.String,
});

class Profil extends Component{
    constructor(props) {
        super(props);
        this.state = {
            firstname: null,
            lastname: null,
            email: null,
            password: null
        };

        let inputRef = React.createRef();
    }

    async sendForm() {
        let validate = this.refs.form.validate();
        if (validate.value.email !== null && validate.value.password !== null) {
            let body = {
                firstname: validate.value.firstname,
                lastname: validate.value.lastname,
                email: validate.value.email,
                password: validate.value.password,
                creator: 'toDetermineLater'
            };
            let response = await UserServices.update('userId', body);
            response.ok ? this.props.navigate.navigate('Profil') : null;
        }
    }


    render() {
        return (
            <View style={styles.container}>
                <Title title={"Mon profil"}/>
                 <Form ref="form" type={profileModel} value={this.state} />
                 <TouchableOpacity style={styles.buttons} onPress={() => this.sendForm()}>
                    <Text style={styles.text}>Enregistrer les changements</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#e3f1ff",
        paddingTop: 70
    },
    buttons: {
        alignSelf: 'center',
        color: 'red',
        backgroundColor: '#36ff97',
        margin: 10,
        padding: 10,
        borderRadius: 20,
    },
    text: {
        color: '#374059',
        fontWeight: 'bold'
    },
});

export default Profil;
