import React, {Component} from 'react';
import { StyleSheet, View, Animated, TextInput, Button, TouchableOpacity, Text, } from 'react-native';
import logo from './../assets/logo.png';
import tcomb from 'tcomb-form-native';
import RecipeServices from "../services/RecipeServices";
import Title from "../components/Title";

// initialisation
const Form = tcomb.form.Form;

// model
const NewRecipeModel = tcomb.struct({
    name: tcomb.String,
    description: tcomb.String,
    preparation_time: tcomb.String,
    category: tcomb.String,
});



class NewRecipe extends Component{
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            preparation_time: null,
            category: null,
            description: null,
            creator: null
        };

        let inputRef = React.createRef();
    }

    async sendForm(){
        let validate = this.refs.form.validate();
         if (validate.value.email !== null && validate.value.password !== null){
            let body = {
                name: validate.value.name,
                preparation_time: validate.value.preparation_time,
                category: validate.value.category,
                description: validate.value.description,
                creator: 'toDetermineLater'
            };
            let response = await RecipeServices.create(body);
            response.ok ? this.props.navigate.navigate('Home') : null;
        }

        // let {name, visual, preparation_time, category, description} = this.state;
        // if (name !== null && visual !== null && preparation_time !== null && category !== null && description !== null){
        //     console.log("to send");
        //     // RecipeServices.create()
        //     // this.props.navigation.navigate('Home');
        // }
    }

    handleChange(e, name){
        this.setState({
        [name]: e.nativeEvent.text
        });
    }


    componentDidMount() {
        // this.containerRef.addEventListener("keypress")
    }

    render() {
        let { newRecipeForm } = this.state;
        return (
            <View style={styles.container}>
                <Title title={"Nouvelle recette"}/>
                <Form ref="form" type={NewRecipeModel} value={this.state} />
                <Button title={"valider"} onPress={() => this.sendForm()}/>
            </View>
        )
    }


}

export default NewRecipe;


const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#e3f1ff"
    },
    input: {
        width: '100%',
        fontSize: 20,
        marginBottom: 20,
        marginLeft:20,
    }

});
