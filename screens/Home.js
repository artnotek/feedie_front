import React, {Component} from 'react';
import  {View,Text,StyleSheet,ScrollView, Button, FlatList} from 'react-native';
import Title from "../components/Title";
import RecipeServices from "../services/RecipeServices";
import RecipeBox from "../components/RecipeBox";
import { updateUserId } from "../actions/login.actions";
import {connect} from 'react-redux';


class Home extends Component{
    constructor(props) {
        super(props);

        this.state = {
            recipes: [],
            userId: null
        }
    }

    async componentDidMount() {
        let {userId} = this.props;
        let response = await RecipeServices.listAll();
        if(response.ok){
            let data = await response.json();
            this.setState({recipes: data.recipe});
        }
        this.props.updateUserId(userId);
        this.setState({userId: 'toDetermineLater'});
    }

    render() {
        let {recipes} = this.state;
        let { navigation } = this.props;
        return (
            <ScrollView style={styles.container}>


                <Title title={"Les dernières créations"}/>

                <FlatList
                    data={recipes}
                    showHorizontalScrollIndicator={false}
                    backgroundColor={"#FFF"}
                    // keyExtractor={(item, index) => 'key'+index}
                    // keyExtractor={(item) => console.log(item)}
                    keyExtractor={item => item._id}
                    renderItem={({item}) => <RecipeBox data={item} navigation={navigation}/>}
                />


            </ScrollView>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#e3f1ff",
        paddingTop: 70
    }
});


const mapStateToProps = state => {
    return {
        userId: state.userId
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateUserId: userId => {dispatch(updateUserId(userId))},
    }
};



export default connect(mapStateToProps, mapDispatchToProps)(Home);
