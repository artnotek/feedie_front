import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';
import recipeimg from '../assets/recipes/recipe.jpg'
import Helpers from "../helpers/Helpers";
import Icon from 'react-native-vector-icons/FontAwesome';


class RecipeBox extends Component{
    constructor(props) {
        super(props);
    }


    details(){

        let {navigation, data} = this.props;
        navigation.navigate('Details', {data: data});
    }

    render() {
        let {horizontal} = this.props;
        let {id, name, visual, description, preparation_time, creation_date,  like_numbers, category, firstname, recipes, avatar, creator} = this.props.data;
        // let {id, name } = this.props.data;
        return (
            // le tableau de style veut dire qu'il va merger les
            <TouchableOpacity onPress={() => this.details()}>
                <View style={ styles.container}>

                    {/*<Image source={{uri: visual}} style={styles.headerImage} resizeMode={"cover"}/>*/}
                    <Image source={{uri: recipeimg}} style={styles.headerImage} resizeMode={"cover"}/>
                    <View style={styles.body}>
                        <View style={styles.miniInfo}>
                            <Text style={styles.title}>{name}</Text>
                            <Text style={styles.isolate}>{category}</Text>
                        </View>
                        <Text style={styles.creatorName}>De: {creator}</Text>
                        <View style={styles.miniInfo}>
                            <Text style={styles.text}>{Helpers.extractDayFromDate(creation_date)}/{Helpers.extractMonthFromDate(creation_date)}</Text>
                            <Text style={styles.isolateText}><Icon name={"hourglass"} size={13} color={'#717575'}/> {preparation_time} mins</Text>
                        </View>
                        <Text style={styles.text}>{description.replace(/(<([^>]+)>)/ig, '').substr(0,120) + "..."}</Text>
                        <Text style={styles.text}><Icon name={"heart"} size={15} color={'#ff6f52'}/> {like_numbers}</Text>
                    </View>
                </View>
            </TouchableOpacity>

        )
    }

}

export default RecipeBox;

const styles = StyleSheet.create({

    container: {
        marginBottom: 20,
        marginHorizontal: 10,


        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        // justifyContent: 'space-between'
    },
    headerImage: {
        borderTopLeftRadius: 20,
        // borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        // borderBottomRightRadius: 20,
        // height: 120,
        width: '37%',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: 0.08,
        shadowRadius: 20,
        // elevation: 5,
    },
    body: {
        // marginRight: 13,
        display: "flex",
        justifyContent: "center",
        height: 150,
        backgroundColor: "#FFF",
        // borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        // borderBottomStartRadius: 20,
        borderBottomEndRadius: 20,
        width: '55%',
        paddingLeft: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: 0.08,
        shadowRadius: 20,
        // elevation: 5,
        // position: "relative",
    },
    creatorName:{
        fontSize: 14,
        fontWeight: '200'
    },
    miniInfo:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    text:{
        fontSize: 14,
        fontWeight: 'normal',
    },
    title: {
        fontSize: 18,
        fontWeight: "600",
    },
    isolate: {
        fontSize: 18,
        fontWeight: "600",
        alignSelf: 'center',
        paddingRight: 40,
        color: '#33d87f'
    },
    isolateText: {
        fontSize: 14,
        alignSelf: 'center',
        paddingRight: 70
    },
    subtitle:{
        fontSize: 16,
        color: "#B0B0B0"
    },
    dateBox: {
        position:'absolute',
        left: 0,
        top: -30,
    },
    containerHorizontal: {
        width: 300
    }
});
